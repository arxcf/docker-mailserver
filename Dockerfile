FROM tvial/docker-mailserver:release-v6.2.0

RUN useradd -ms /bin/bash nodejs
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs
COPY scripts /scripts
WORKDIR /scripts
RUN npm install
RUN chmod +x mail-hook.js mail-ws.js
RUN chown -R nodejs:nodejs /scripts
RUN sed -i 's/zen\.spamhaus\.org\*3//' /etc/postfix/main.cf 
RUN sed -i 's/reject_rbl_client\ zen\.spamhaus\.org,\ //' /etc/postfix/main.cf
#Adding node hook in postfix
COPY config/master.cf /etc/postfix/master.cf
#Adding ovh load balancer to spf whitelist
COPY config/policyd-spf.conf /etc/postfix-policyd-spf-python/policyd-spf.conf
#Adding webservice to manage postfix account
COPY config/ws-supervisor.conf /etc/supervisor/conf.d/ws-supervisor.conf
#Initializing postfix with one account to allow dovecot to start
COPY config/postfix-init-supervisor.conf /etc/supervisor/conf.d/postfix-init-supervisor.conf
#Initializing webhook conf with env variable from postfix user
COPY config/webhook-init-supervisor.conf /etc/supervisor/conf.d/webhook-init-supervisor.conf