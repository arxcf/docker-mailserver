- Adding a webservice API to [tvial/docker-mailserver](https://hub.docker.com/r/tvial/docker-mailserver/). Api doc is available on port 3000/api-docs

- Initializing postfix with a default account to enable dovecot startup

- Example:

        sudo docker run -d --rm \
            --name=mail \
            --hostname toto \
            --domainname titi.fr \
            -p 25:25 \
            -p 143:143 \
            -p 587:587 \
            -p 993:993 \
            -p 3000:3000 \
            -v /home/chaya/Projects/mail/data:/var/mail \
            -v /home/chaya/Projects/mail/config:/tmp/docker-mailserver \
            arxcf/mailserver

 - unset JMX_PORT
 - kafka-console-consumer --bootstrap-server localhost:19092 --topic dev-mail.arxcf.com --from-beginning
 - kafka-topics --bootstrap-server localhost:19092 --delete --topic dev-mail.arxcf.com
 - kafka-topics --bootstrap-server localhost:19092 --list