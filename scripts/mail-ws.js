#!/usr/bin/env node

const bodyParser = require('body-parser'); 
const express = require('express');
const app = express();
const { exec } = require('child_process');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
 
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(bodyParser.json());

app.get('/accounts', function (req, res) {
    exec('listmailuser', (err, stdout, stderr) => {
        if (err) {
            console.log(err);
            res.status(500);
            res.send(err);
        } else {
            res.json(stdout.split('\n')
                .filter(mail => mail && mail.length > 1)
                .map(mail => { return {'mail': mail} })
            );
        }
    });
});

app.put('/accounts/:id', function (req, res) {
    exec(`updatemailuser ${req.params.id} ${req.body.password}`, (err, stdout, stderr) => {
        if (err) {
            console.log(err);
            res.status(500);
            res.send(err);
        } else {
            res.send();
        }
    });
});

app.post('/accounts', function (req, res) {
    exec(`addmailuser ${req.body.mail} ${req.body.password}`, (err, stdout, stderr) => {
        if (err) {
            console.log(err);
            res.status(500);
            res.send(err);
        } else {
            res.status(201);
            res.send();
        }
    });
});

app.delete('/accounts/:id', function (req, res) {
    exec(`delmailuser -y ${req.params.id}`, (err, stdout, stderr) => {
        if (err) {
            console.log(err);
            res.status(500);
            res.send(err);
        } else {
            res.send();
        }
    });
});

app.all('*', function(req, res) {
    res.redirect("/api-docs");
});

app.listen(3000);