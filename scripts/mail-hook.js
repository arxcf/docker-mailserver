#!/usr/bin/env node

const { v4: uuidv4 } = require('uuid');
const winston = require('winston');
const FormData = require('form-data');
const simpleParser = require('mailparser').simpleParser;
const { mainModule } = require('process');
require('winston-daily-rotate-file');

const customFormat = winston.format.printf((info) => {
  if (info.meta && info.meta instanceof Error) {
    return `${info.timestamp} ${info.level} ${info.message} : ${info.meta.stack}`;
  }
  return `${info.timestamp} ${info.level}: ${info.message}`;
});

const transport = new (winston.transports.DailyRotateFile) ({
  dirname: `${__dirname}/logs`,
  filename: 'mail-hook-%DATE%.log',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '10',
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.splat(),
    customFormat
  )
});

const logger = winston.createLogger({ transports: [ transport ] });

const ID = uuidv4();

let config;
try {
  config = require('./config.json');
} catch(error) {
  logger.error(`Can't start mail hook, no config detected, you must provide a config.json file for mail prefix mapping`);
}

if (config) {
  main();
}

function main() {
  if (process.argv.length > 2) {
    processMailFromHook();
  } else {
    processMailFromLocalFile('_local_', './test/mail-big.txt');
  }
}

function processMailFromLocalFile(prefix, file) {
  logger.info(`${ID} - Filtering email from local file ${file}`);
  const mail = require('fs').readFileSync(file, 'utf8');
  let c = searchCorrespondence('_local_');
  if (c) {
    postWebservice(mail, 'arx', c);
  }
}

function processMailFromHook() {
  logger.info(`${ID} - Filtering email from ${process.argv[2]} to ${process.argv.slice(3).join(' ')}`);
  let mail = '';

  const rl = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
  });
  
  rl.on('line', cmd => {
    mail += `${cmd}\n`;
  });
  
  rl.on('close', () => {
    const groups = /^(?<address>[^@]+)@(?<domain>[^@]+)$/gm.exec(process.argv[3].trim()).groups,
        prefix = groups.address.substring(0, groups.address.indexOf('_')) || groups.address;

    let c = searchCorrespondence(prefix);
    if (c) {
      postWebservice(mail, prefix, c);
    }
  });
  
  rl.resume();
}

function searchCorrespondence(prefix) {
  if (config[prefix]) {
    logger.info(`${ID} - Found correspondance for '${prefix}': ${config[prefix].crm} / ${config[prefix].dm}`);
    return config[prefix];
  } else {
    logger.error(`No configuration found in config.json for prefix '${prefix}'`);
  }
}

async function postWebservice(mail, prefix, servers) {
  let parsed = await simpleParser(mail);
  const axios = require('axios').default;
  const attachments = parsed.attachments;
  parsed.attachments = null;
  logger.info(`${ID} - Creating activity ...`);
  axios.post(servers.crm, parsed, {
    headers: {
      'Authorization': `Basic ${servers.crmToken}`
    }
  }).then( response => {
    const activities = response.data;
    activities.forEach(activity => {
      logger.info(`${ID} - Activity created: ${activity.id}`);
      var formData = new FormData();
      formData.append('sourceId', activity.id);
      formData.append('prefix', prefix);
      attachments.forEach( attachment => { 
        logger.info(`${ID} - Sending attachments ${attachment.filename} for activity: ${activity.id}`);
        formData.append("files", attachment.content, {
          header: attachment.headers,
          knownLength: attachment.size,
          filename: attachment.filename,
          contentType: attachment.contentType
        })
      });
      let headers = formData.getHeaders();
      headers.Authorization = `Basic ${servers.dmToken}`

      axios.post(servers.dm, formData, {
          headers: headers
      })
      .then( () => logger.info(`${ID} - Attachments sent successfully`))
      .catch( error => logger.error(`${ID} - Error when uploading attachments: ${error}`));
    });
  })
  .catch( error => logger.error(`${ID} - Error when creating activity: ${error}`));
}